const json2csv = require('json2csv');

exports.get = function(req, res) {

	//var fields = Object.keys(Author.schema.obj);
	const fields = [
		'handle',
		'phone',
		'addressWeekday',
		'addressWeekend',
		'birthDate',
		'gender',
		'medicalHistory',
		'allergy',
		'bio',
		'social.youtube',
		'social.twitter',
		'social.facebook',
		'social.linkedin',
		'social.instagram'
	];

	const csv = json2csv({ data: '', fields: fields });

	res.set("Content-Disposition", "attachment;filename=profile.csv");
	res.set("Content-Type", "application/octet-stream");

	res.send(csv);

};