import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import isEmpty from '../../validation/is-empty';
import DataGrid, {
  Column,
  FilterRow,
  Grouping,
  GroupPanel,
  Pager,
  Paging,
  Selection
} from 'devextreme-react/ui/data-grid';

class ProfileItem extends Component {
  onDeleteClick(id) {
    this.props.deleteExperience(id);
  }

  render() {
    const { profiles} = this.props;
    console.log("====== " + profiles);
    // profiles.map(profile => (
    //     <tr key={profile._id}>
    //              <td>{profile.user.name}</td>
    //              <td>{profile.phone}</td>
    //              <td>{profile.gender}</td>
    //              <td>
    //              <button
    //                   onClick={this.onDeleteClick.bind(this, profile._id)}
    //                   className="btn btn-danger"
    //               >
    //                 Delete
    //               </button>
    //             </td>
    //           </tr>
    // ));
    return (
        <div>
          <h4 className="mb-4">Experience Credentials</h4>
          <table className="table">
            <thead>
            <tr>
              <th>Nama</th>
              <th>Phone</th>
              <th>Jenis Kelamin</th>
              <th />
            </tr>
            {profiles}
            </thead>
          </table>
        </div>
    );
  }
  // render() {
  //   const { profile } = this.props;
  //   const {getProfile} = this.props;
  //
  //   console.log('user ===== ' + profile.user.name);
  //   console.log('phone ===== ' + profile.phone);
  //   return (
  //       <DataGrid
  //           dataSource={profile}
  //           // keyExpr={''}
  //           allowColumnReordering={true}
  //       >
  //         <GroupPanel visible={true} />
  //         <Grouping autoExpandAll={true} />
  //         <FilterRow visible={true} />
  //         <Selection mode={'multiple'} />
  //
  //         <Column
  //             dataField={'profile.user.name'}
  //             caption={'User'}
  //             allowSorting={false}
  //             allowFiltering={false}
  //             allowGrouping={false}
  //             allowReordering={false}
  //             width={100}
  //             groupIndex={0}
  //         />
  //         <Column dataField={profile.phone}
  //                 caption={'Phone'}/>
  //         <Column dataField={profile.addressWeekday}
  //                 sortOrder={'addressWeekday'} />
  //         <Column dataField={'Address Weekend'}
  //                 caption={profile.addressWeekend}
  //                 // groupIndex={'addressWeekend'}
  //         />
  //         {/*<Column*/}
  //             {/*dataField={'date'}*/}
  //             {/*dataType={'date'}*/}
  //             {/*selectedFilterOperation={'>='}*/}
  //             {/*filterValue={'2013/04/01'}*/}
  //             {/*width={150}*/}
  //         {/*/>*/}
  //         {/*<Column*/}
  //             {/*dataField={'amount'}*/}
  //             {/*format={'currency'}*/}
  //             {/*selectedFilterOperation={'>='}*/}
  //             {/*filterValue={1000}*/}
  //             {/*width={100}*/}
  //         {/*/>*/}
  //
  //         <Pager allowedPageSizes={[5, 10, 20]} showPageSizeSelector={true} />
  //         <Paging defaultPageSize={10} />
  //       </DataGrid>
  //
  //   );
  // }
}

ProfileItem.propTypes = {
  profile: PropTypes.object.isRequired
};

export default ProfileItem;
