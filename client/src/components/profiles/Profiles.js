import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
// import Spinner from '../common/Spinner';
import Spinner from '../common/Spinner';
import {deleteAccount, getProfiles} from '../../actions/profileActions';

class Profiles extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            isAdmin: '',
            errors: {}
        };
    }

    componentDidMount() {

        if (this.props.auth.isAuthenticated) {
            if (this.props.auth.user.email === 'admin@admin.com') {
                this.props.history.push('/dashboard');
            } else {
                this.props.getProfiles();
                // this.props.history.push('/dashboard');
                // this.props.getProfiles();
            }
        }
    }

    // componentWillReceiveProps(nextProps) {
    //     if (nextProps.auth.isAuthenticated) {
    //         if (this.props.match.params.handle ===  'admin') {
    //             // this.props.history.push('/profiles');
    //             this.props.getProfiles(this.props.match.params.handle);
    //         }else {
    //             this.props.history.push('/dashboard');
    //         }
    //     }
    //     if (nextProps.errors) {
    //         this.setState({ errors: nextProps.errors });
    //     }
    // }

    onEditClick(id) {
        this.props.getProfiles(id);
    }

    onDeleteClick() {
        this.props.deleteAccount();
    }

    render() {
        const {profiles, loading} = this.props.profile;
        let profileItems;

        if (profiles === null || loading) {
            profileItems = <Spinner/>;
        } else {
            if (profiles.length > 0) {
                profileItems = profiles.map(profile => (
                    <tr key={profile._id}>
                        <td>{profile.handle}</td>
                        <td>{profile.phone}</td>
                        <td>{profile.gender}</td>
                        <td>{profile.addressWeekday}</td>
                        <td>{profile.addressWeekend}</td>
                        <td>{profile.medicalHistory}</td>
                        <td>{profile.allergy}</td>
                        <td>
                            <Link to="/edit-profile" className="btn btn-light">
                                <i className="fas fa-edit text-info mr-1"/> Edit
                            </Link>
                            <button
                                onClick={this.onDeleteClick.bind(this)}
                                className="btn btn-light"
                            >
                              <i className="fa fa-trash text-info mr-1"/>
                               Delete
                            </button>
                        </td>
                    </tr>
                ))
            } else {
                profileItems = <h4>No profiles found...</h4>;
            }
        }

        return (
            <div>
                <h4 className="mb-4">Customers</h4>
                <Link to="/create-profile-admin" className="btn  btn-light">
                    <i className="fas fa-plus text-info mr-1"/> Tambah Customers
                </Link>
                <Link to="/upload-profile" className="btn  btn-light">
                    <i className="fas fa-upload text-info mr-1"/> Upload Customers
                </Link>
                <p/>
                <table className="table">
                    <thead>
                    <tr>
                        <th>Nama Customers</th>
                        <th>No Telp</th>
                        <th>Jenis Kelamin</th>
                        <th>Alamat Weekday</th>
                        <th>Alamat Weekend</th>
                        <th>Riwayat Sakit</th>
                        <th>Alergi</th>
                        <th>Aksi</th>
                        <th/>
                    </tr>
                    {profileItems}
                    </thead>
                </table>
            </div>
        );
    }
}

Profiles.propTypes = {
    getProfiles: PropTypes.func.isRequired,
    deleteAccount: PropTypes.func.isRequired,
    profile: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    profile: state.profile,
    auth: state.auth
});

export default connect(mapStateToProps, {getProfiles, deleteAccount})(Profiles);
