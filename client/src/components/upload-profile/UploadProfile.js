import React from 'react';
import Download from '@axetroy/react-download';
import {Link} from 'react-router-dom';

export default () => {
  return (

      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h3 className="display-4 text-center">Upload Customers</h3>
              <p>Download template csv profile
                <Download file="profile.csv" content="handle,phone,addressWeekday,addressWeekend,birthDate,gender,medicalHistory,
                      allergy,bio,social.youtube,social.twitter,social.facebook,social.linkedin,social.instagram">
                  <button type="button" className="btn btn-info" >Download template</button>
                </Download> </p>
              <form action="api/uploadprofile" method="POST" encType="multipart/form-data">
                <div className="form-group">
                  <div className="input-group input-file" >
                    <input type="file" className="form-control" name="file" accept="*.csv"
                           placeholder='Choose a file...'/>
                  </div>
                </div>
                <button type="submit" className="btn btn-primary mr-1">Upload</button>
                <Link to="/profiles" id="cancel" name="cancel"
                   className="btn btn-secondary">Cancel</Link>
              </form>
            </div>
          </div>
        </div>
      </div>
  );
};
