import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from '../../validation/is-empty';

class ProfileAbout extends Component {
  render() {
    const { profile } = this.props;

    // Get first name
    const firstName = profile.user.name.trim().split(' ')[0];
    const birtOfDate = profile.birthDate;

    // Skill List
    // const skills = profile.skills.map((skill, index) => (
    //   <div key={index} className="p-3">
    //     <i className="fa fa-check" /> {skill}
    //   </div>
    // ))

    // Medical List
    const medicalHistory = profile.medicalHistory.map((medicalHistory, index) => (
        <div key={index} className="p-3">
          <i className="fa fa-check" /> {medicalHistory}
        </div>
    ));

    return (
      <div className="row">
        <div className="col-md-12">
          <div className="card card-body bg-light mb-3">
            <h3 className="text-left text-info">Tanggal Lahir</h3>
            {isEmpty(profile.birthDate) ? (
                <span>{birtOfDate} does not exist</span>
            ) : (
                <span>{profile.birthDate}</span>
            )}
            <hr />
            <h3 className="text-left text-info">Jenis Kelamin</h3>
            {isEmpty(profile.gender) ? (
                <span>{firstName} does not have a gender</span>
            ) : (
                <span>{profile.gender}</span>
            )}
            <hr />
            <h3 className="text-left text-info">No Telp</h3>
              {isEmpty(profile.phone) ? (
                  <span>{firstName} does not have a phone</span>
              ) : (
                  <span>{profile.phone}</span>
              )}
            <hr />
            <h3 className="text-left text-info">Alamat Weekday</h3>
            {isEmpty(profile.addressWeekday) ? (
                <span>{birtOfDate} does not exist</span>
            ) : (
                <span>{profile.addressWeekday}</span>
            )}
            <hr />
            <h3 className="text-left text-info">Alamat Weekend</h3>
            {isEmpty(profile.addressWeekend) ? (
                <span>{birtOfDate} does not exist</span>
            ) : (
                <span>{profile.addressWeekend}</span>
            )}
            <hr />
            <h3 className="text-left text-info">{firstName}'s Bio</h3>
              {isEmpty(profile.bio) ? (
                <span>{firstName} does not have a bio</span>
              ) : (
                <span>{profile.bio}</span>
              )}
            <hr />
            <h3 className="text-left text-info">Riwayat Sakit</h3>
            <div className="row">
              <div className="d-flex flex-wrap justify-content-center align-items-center">
                {medicalHistory}
              </div>
            </div>
            <h3 className="text-left text-info">Alergi</h3>
                {isEmpty(profile.allergy) ? (
                    <span>{birtOfDate} does not exist</span>
                ) : (
                    <span>{profile.allergy}</span>
                )}
          </div>
        </div>
      </div>
    );
  }
}

ProfileAbout.propTypes = {
  profile: PropTypes.object.isRequired
};

export default ProfileAbout;
